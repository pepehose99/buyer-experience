---
  title: What are microservices? | GitLab
  description: Microservices are a modern software architecture design where an application is split into many small parts that can work independently, allowing teams to innovate faster and achieve massive scale.
  components:
    - name: topics-header
      data:
        title: What are microservices?
        two_col: true
        block:
            - metadata:
                id_tag: what-are-microservices
              text: |
                A microservices architecture splits an application into many small services allowing teams to innovate faster and achieve massive scale.
              link_text: Join our cloud transformation webcast
              link_href: /webcast/cloud-native-transformation/
              data_ga_name: Join our cloud transformation webcast
              data_ga_location: header
    - name: 'copy-media'
      data:
        block:
          - header: Moving from monolith to microservices
            text: |
                You can understand [microservices architecture](https://about.gitlab.com/direction/microservices/) by comparing it to a legacy monolith application architecture. With monolithic architecture, all the components are part of a single unit. It develops, deploys and scales everything together. Teams must write the app in a single language with a single runtime.

                Different teams working on different app components must coordinate, so they don't affect each other's work. For example, one part of the app may depend on a module that needs a specific version, say 1.8, while another team needs the same module but requires the 1.7 version because 1.7 is incompatible with another dependency. In a monolithic app, you have to pick one or the other. Similarly, monolithic apps deploy everything as a single application, requiring testing and deploying code together.

                With microservices, each component is broken out and deployed individually as services and the services communicate with each other via API calls.
            image:
              image_url: /nuxt-images/topics/monolith-vs-microservices-large.png
              alt: A monolith vs microservice architecture
            column_size: 8
            column: true
            hide_horizontal_rule: true
    - name: 'copy'
      data:
        block:
          - header: Microservice core components
            text: |
                Microservices maximize application reliability and deployment velocity. This is particularly important in a marketplace that's moving and evolving faster than ever. Microservices' containerized architecture moves apps anywhere without altering or disrupting the environment, facilitating speed and reducing downtime.

                Let's look at microservices' core components:

                1. **Clients**: Client apps usually must consume functionality from multiple microservices and require frequent updates. 
                1. **Databases**: A service API updates microservices databases by carrying all remote services supporting inter-process communications for different stacks.
                1. **API gateway**: An API gateway is a microservices design pattern that's a vital single app entry point for routing requests and protocol translation. 
                1. **Identity providers**: An identity microservice must permit server-to-server and user-driven access to identity data. 
                1. **Messaging formats**: Microservices communicate with synchronous or asynchronous microservice architecture patterns. 
                1. **Static content**: Clients receive static content via cloud-based storage services and content delivery networks. 
                1. **Management**: This component allows business users to configure services during run-time.   
                1. **Service discovery**: A service discovery mechanism is required to request service.
            column_size: 8
            column: true
            hide_horizontal_rule: true
    - name: 'copy-media'
      data:
        block:
          - header: Microservice characteristics
            text: |
              [Martin Fowler's quintessential article on microservices](https://martinfowler.com/articles/microservices.html) defines nine components that most microservice architectures have in common. .

              ### 1. Componentization via Services
              Microservice's nature dictates breaking down each component by service. As such, it can designate, deploy, refine and redeploy each service independently without affecting the environment. This means it's usually possible to change a service instead of redeploying an entire app. 

              ### 2. Organized around Business Capabilities
              With the monolithic approach, tech teams focused separately on tech-related capabilities such as UI, databases and server-side logic. On the other hand, microservices organize cross-functional teams around business capabilities and priorities. Each team designs specific products based on individual services that communicate using a message bus.

              ### 3. Products not Projects
              In the past, app developers used the project model that tasked teams to build software, which then went to a maintenance organization upon completion. Microservices architecture favors a team owning the product over its lifetime. 

              This allows developers to interact with their products in new ways, seeing how they behave in production and increasing user contact. This benefits engineers and businesses to increase collaboration and understand each other's fields. 

              ### 4. Smart endpoints and dumb pipes
              Microservices have a lot in common with the traditional UNIX system. They receive a request, process it and generate an appropriate response. Fowler refers to this approach as "smart endpoints and dumb pipes." The infrastructure is usually dumb as it serves solely as a message router, and all the smarts reside at the endpoints producing and consuming endpoints.

              ### 5. Decentralized Governance
              Decentralized governance is microservice architecture's default structure because single-technology platforms often lead to over-standardization. A major advantage of microservices vs. monoliths is using different languages where they're best suited. For example, Spring Boot microservices can build an app for one component, with Spring Cloud comprising another.  

              ### 6. Decentralized Data Management
              Most microservices allow each component to manage its own database from decentralized data management. You can always use the best data store for a specific project while removing the time-consuming task of shared database upgrades. 

              ### 7. Infrastructure Automation
              CI/CD experts use infrastructure automation in microservices. It lessens developers' workloads and significantly improves deployment timeline efficiency.

              ### 8. Design for failure
              Like the best businesses, microservices have resilience in mind. As unique and diverse services must communicate, failure is likely. This is microservices' main disadvantage compared to monoliths because the solution requires additional complexity.

              Sophisticated monitoring and logging setups are necessary to prevent failure from impacting consumers. While this requires more work for engineers, it means fail resistance is well-developed.

              ### 9. Evolutionary Design
              With such a fast-paced tech industry, evolutionary design is no longer a luxury; it's a necessity. More new electronic device types hit the market each year, and your apps must be ready to facilitate them. Microservices' deconstructed design means you can give apps a makeover without redeployment.  

              Fowler went into more detail about each of these components in this talk from GOTO.
            video:
              video_url: https://www.youtube-nocookie.com/embed/wgdBVIX9ifA
            column_size: 8
            column: true
            hide_horizontal_rule: true
    - name: copy
      data:
        block:
          - header: Microservice architecture use cases
            text: |
              Microservices designs are agile and flexible and enable minimal downtime for maintenance, repairs and updates.
              
              Some key use cases for microservices include:
              1. **Data processing**: Apps running on microservice architecture can handle more concurrent requests in less time, making them more efficient.
              1. **Website migration**: Microservices avoid site UI, structure and/or domain site migration downtime.
              1. **Large-scale media**: Platforms can handle an extreme number of requests for different subdomains without errors or delays.
              1. **Invoices and transactions**: Microservices can help make transactions more robust with reduced app failure, allowing companies to scale up without redeploying. 
              1. **Outdated systems**: Developing a new containerized system is more efficient and effective than updating old, clunky monoliths. 
            column_size: 8
            column: true
            hide_horizontal_rule: true

    - name: copy
      data:
        block:
          - header: Engineering benefits of microservices
            text: |
              [Implementing a microservices architecture](/blog/2019/06/17/strategies-microservices-architecture/){:data-ga-name="Microservice architecture"}{:data-ga-location="body"}, or decomposing a legacy monolith into microservices, can increase velocity, flexibility and scalability, often at simplicity's cost. Monoliths are straightforward to build, deploy and debug, but hard to scale. While a microservice architecture is more complex, there are several benefits for engineers:

              ### 1. Independent deployment
              Each microservice must be a full-stack to function correctly. Development teams can easily fix errors on a single microservice without impacting the whole environment. 

              ### 2. Scalability
              Teams building apps with microservices architecture can change each service without disrupting the rest of the app. Programmers can work in parallel to identify hot services, update functionality and scale selected services.   

              ### 3. Tech stack freedom
              Reliance on the singular tech stack is one of monolithic architecture's most limiting features. With microservices, developers can build an app using the programming language, framework, databases and front-end and backend tools. This mitigates compromising on a jack-of-all-trades and master of none standardized stack. 

              ### 4. Fault isolation
              A single fault often means an entire monolith app fails. Microservices isolate faults to their components, so the whole application isn't affected.  

              ### 5. Productivity
              Microservices reduce silos and promote cross-functional teamwork for today's distributed and remote workforces. They can be tested independently while developers handle other aspects, streamlining quality assurance. 
            metadata:
              id_tag: engineering-benefits-of-microservices
            hide_horizontal_rule: true
            column_size: 8
    - name: copy
      data:
        block:
          - header: Business value of microservices
            text: |
              With business value, microservices have a lot of benefits compared to monoliths. They promote a symbiotic relationship between developers and business leaders, leading to better outcomes. Microservices promote:

              1. **Faster pace of innovation.** Their evolutionary design makes it easy to change and upgrade application components.
              1. **Greater stability/resiliency.** Microservices practically eliminate downtime because, unlike monolithic architecture, services are separate and developers can update one component at a time.
              1. **Software can scale to keep up with business demand.** Development teams can work on single components without impacting the entire environment.
              1. **Lower costs and better revenue.** Microservices need less overall infrastructure, minimize downtime and improve stability for enhanced customer experience and reduce app prototype to deployment timeframes.  
            metadata:
              id_tag: business-value-of-microservices
            column_size: 8
            hide_horizontal_rule: true
    - name: copy
      data:
        block:
          - header: Using GitLab with microservices
            text: |
              With GitLab, you can commit your code and have the tools you need in a single application. No more stitching together 10 tools for every project.

              Using a [DevOps platform](/topics/devops-platform/){:data-ga-name="Devops platform"}{:data-ga-location="body"} to manage your microservices helps you avoid information silos. Increasing visibility among teams and making handoffs easier leads to a faster DevOps lifecycle while also ensuring that your projects deploy and remain stable.

              A few ways GitLab simplifies microservice orchestration include:

              1. [Built-in CI/CD](/stages-devops-lifecycle/continuous-integration/){:data-ga-name="Built in CI/CD"}{:data-ga-location="body"}: As Fowler points out, infrastructure automation using continuous delivery and deployment is necessary for microservices. GitLab's built-in CI/CD is ideal for businesses looking to leverage microservices.
              1. [Built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html){:data-ga-name="Built in container registry"}{:data-ga-location="body"} and a robust [Kubernetes integration](/solutions/kubernetes/){:data-ga-name="Kubernetes integration"}{:data-ga-location="body"}: While microservices architecture can be used with legacy VM technology, containers and Kubernetes make building microservices significantly easier. GitLab is designed to work well with Kubernetes.
              1. [Built-in Monitoring](/stages-devops-lifecycle/monitor/){:data-ga-name="Built in monitoring"}{:data-ga-location="body"}: Monitoring is critical to a successful operation. GitLab's monitoring capabilities leveraging Prometheus make GitLab ideal for microservices.
              1. [Multi-project pipelines](/blog/2018/10/31/use-multiproject-pipelines-with-gitlab-cicd/){:data-ga-name="Multi project pipelines"}{:data-ga-location="body"} support running pipelines with cross-project dependencies.
              1. Monorepo support with the ability to [run a pipeline only when code a specific directory changes](https://docs.gitlab.com/ee/ci/yaml/#only-and-except-simplified){:data-ga-name="Only and except"}{:data-ga-location="body"}.
              1. [Group-level Kubernetes clusters](https://docs.gitlab.com/ee/user/group/clusters/){:data-ga-name="Group-level kubernetes"}{:data-ga-location="body"} allow multiple projects to integrate with a single Kubernetes cluster.
            metadata:
              id_tag: using-gitlab-with-microservices
            hide_horizontal_rule: true
            column_size: 8
    - name: copy
      data:
        block:
          - header: GitLab key benefits
            text: |
              GitLab is a single DevOps platform attracting top developers looking to collaborate and improve. Below are some key benefits of becoming a GitLab advocate:

              * **No switching between apps** - Developers don't need to constantly context-switch, making it easier to focus and stay on task. Additionally, you can easily link issues to work, increase productivity and eliminate software engineers' main bugbears.
              * **Reduce your workload** - GitLab makes it easier to automate tasks so that you can focus on more demanding, results-based activities. Repetitive manual tasks take the joy out of a developer's workday. Automate tasks to improve work satisfaction and productivity. 
              * **Collaboration and transparency** - Collaboration and transparency are key benefits of the GitLab platform. Abject transparency between developer and customer facilitates collaboration for quick, easy fixes and less communication. 
              * **Be part of the community** - All developers are free to contribute to GitLab's open-source core to improve the app. It's a warm, friendly and highly responsive community that welcomes new community members and helps them learn the ropes.  
              * **Learn cutting-edge software development practices** - Coding and app development is one of the fastest-growing fields in an increasingly fast-paced world. GitLab updates its community on all the latest developments and helps them learn best practices.
            metadata:
              id_tag: getting-even-better
            hide_horizontal_rule: true
            column_size: 8
    - name: topics-cta
      data:
        title: Start your cloud native transformation
        subtitle: Build, test, deploy, and monitor your code from a single application.
        text: |
          Hear how Ask Media Group migrated from on-prem servers to the AWS cloud with GitLab tools and integrations. Join us and learn from their experience.
        column_size: 8
        cta_one:
          text: Save your spot!
          link: /webcast/cloud-native-transformation/
          data_ga_name: Save your spot
          data_ga_location: body
    - name: copy-resources
      data:
        title: Resources
        block:
          - resources:
              video:
                header: Videos
                links:
                  - text: Mastering Chaos - A Netflix Guide to Microservices
                    link: https://www.youtube.com/watch?v=CZ3wIuvmHeM
                    data_ga_name: Mastering Chaos - A Netflix Guide to Microservices
                    data_ga_location: body
                  - text: Guilt's move from monolith to microservices
                    link: https://www.youtube.com/watch?v=C4c0pkY4NgQ
                    data_ga_name: Guilt's move from monolith to microservices
                    data_ga_location: body
              article:
                header: Articles
                links:
                  - text: How to break a Monolith into Microservices
                    link: https://martinfowler.com/articles/break-monolith-into-microservices.html
                    data_ga_name: How to break a Monolith into Microservices
                    data_ga_location: body
                  - text: Evolution of business logic from monoliths through microservices, to functions
                    link: https://read.acloud.guru/evolution-of-business-logic-from-monoliths-through-microservices-to-functions-ff464b95a44d
                    data_ga_name: Evolution of business logic from monoliths through microservices, to functions
                    data_ga_location: body
    - name: featured-media
      data:
        header: Suggested Content
        column_size: 4
        media:
          - title: 'Trends in Version Control Land: Microservices'
            aos_animation: fade-up
            aos_duration: 500
            text: |
              The benefits and drawbacks of microservices and how to decide if it is right for your team.
            link:
              text: Learn more
              href: /blog/2016/08/16/trends-in-version-control-land-microservices/
            image:
              url: /nuxt-images/blogimages/trends-in-version-control-land-microservices-cover.jpg
              alt:
          - title: "It's raining repos: The microservices repo explosion, and what we're doing about it"
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Microservices have spawned an explosion of dependent projects with multiple repos, creating the need for an integrated solution - we're working on it right now.
            link:
              text: Learn more
              href: /blog/2018/11/26/microservices-integrated-solution/
            image:
              url: /nuxt-images/blogimages/microservices-explosion.jpg
              alt:
          - title: Implementing microservices architectures and deployment strategies
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              Want to dump the monolith and get into microservices? Consider these three methods.
            link:
              text: Learn more
              href: /blog/2019/06/17/strategies-microservices-architecture/
            image:
              url: /nuxt-images/blogimages/microservices-explosion.jpg
              alt:
          - title: How to manage Agile teams with microservices
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLab Groups and Projects can help teams divide work by product or system.
            link:
              text: Learn more
              href: /blog/2019/08/23/manage-agile-teams-with-microservices/
            image:
              url: /nuxt-images/blogimages/agilemultipleteams.jpg
              alt:
          - title: 'From monolith to microservices: How to leverage AWS with GitLab'
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLab recently spent time with Ask Media Group and AWS to discuss how modernizing from self-managed to a cloud native system empowers developers.
            link:
              text: Learn more
              href: /blog/2020/03/24/from-monolith-to-microservices-how-to-leverage-aws-with-gitlab/
            image:
              url: /nuxt-images/blogimages/askmediablog-.jpg
              alt:
          - title: What devs need to know about tomorrow's tech today
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              From 5G to edge computing, microservices and more, cutting-edge technologies will be mainstream soon. We asked more than a dozen DevOps practitioners and analysts which technologies developers need to start to understand today.
            link:
              text: Learn more
              href: /blog/2020/10/21/how-tomorrows-tech-affects-sw-dev/
            image:
              url: /nuxt-images/blogimages/future-of-software-what-developers-need-to-know.png
              alt:
