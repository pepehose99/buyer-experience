---
components:
  - name: copy
    data:
      block:
        - hide_horizontal_rule: true
          no_margin_bottom: true
          text: |
            ## General Support Practices {#general-support-practices}

            ### Differences Between Support Tickets and GitLab Issues {#differences-between-support-tickets-and-gitlab-issues}

            It's useful to know the difference between a support ticket opened through our [Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="support practices header"} vs. [an issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues){data-ga-name="GitLab issues" data-ga-location="support practices header"}.

            For customers with a license or subscription that includes support, always feel free to contact support with your issue in the first instance via [the Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="support practices body"}. This is the primary channel the support team uses to interact with customers and it is the only channel that is based on an SLA. Here, the GitLab support team will gather the necessary information and help debug the issue. In many cases, we can find a resolution without requiring input from the development team. However, sometimes debugging will uncover a bug in GitLab itself or that some new feature or enhancement is necessary for your use-case.

            This is when we create an [issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues){data-ga-name="issues" data-ga-location="support practices body"} - whenever input is needed from developers to investigate an issue further, to fix a bug, or to consider a feature request. In most cases, the support team can create these issues on your behalf and assign the correct labels. Sometimes we ask the customer to create these issues when they might have more knowledge of the issue or the ability to convey the requirements more clearly. Once the issue is created on GitLab.com it is up to the product managers and engineering managers to prioritize and drive the fix or feature to completion. The support team can only advocate on behalf of customers to reconsider a priority. Our involvement from this point on is more minimal.

            #### We don't keep tickets open (even if the underlying issue isn't resolved) {#we-dont-keep-tickets-open-even-if-the-underlying-issue-isnt-resolved}

            Once an issue is handed off to the development team through an issue in a GitLab tracker, the support team will close out the support ticket as Solved _even if the underlying issue is not resolved_. This ensures that issues remain the single channel for communication: customers, developers and support staff can communicate through only one medium.

            ### Issue Creation {#issue-creation}

            Building on the above section, when bugs, regressions, or any application behaviors/actions **not working as intended** are reported or discovered during support interactions, the GitLab Support Team will create issues in GitLab project repositories on behalf of our customers.

            For feature requests, both involving the addition of new features as well as the change of features currently **working as intended**, support will request that the customer create the issue on their own in the appropriate project repos.

            ### Working Effectively in Support Tickets {#working-effectively-in-support-tickets}

            As best you can, please help the support team by communicating the issue you're facing, or question you're asking, with as much detail as available to you. Whenever possible, include:
            - [log files relevant](https://docs.gitlab.com/ee/administration/logs.html#gathering-logs){data-ga-name="gathering logs" data-ga-location="body"} to the situation.
            - steps that have already been taken towards resolution.
            - relevant environmental details, such as the architecture.

            We expect for non-emergency tickets that GitLab administrators will take 20-30 minutes to formulate the support ticket with relevant information. A ticket without the above information will reduce the efficacy of support.

            In subsequent replies, the support team may ask you follow-up questions. Please do your best read through the entirety of the reply and answer any such questions. If there are any additional troubleshooting steps, or requests for additional information please do your best to provide it.

            The more information the team is equipped with in each reply will result in faster resolution. For example, if a support engineer has to ask for logs, it will result in more cycles. If a ticket comes in with everything required, multiple engineers will be able to analyze the problem and will have what is necessary to further escalate to developers if so required.

            #### Please don't use language intended to threaten or harass {#please-dont-use-language-intended-to-threaten-or-harass}
            If your ticket contains language that goes against the [GitLab Community Code of Conduct](/community/contribute/code-of-conduct/){data-ga-name="code of conduct" data-ga-location="body"}, you'll receive the following response and have your ticket closed:

            >Hello,
            >
            >While we would usually be very happy to help you out with any issue, we cannot assist you on this ticket due to the language used not adhering to the [GitLab Community Code of Conduct](/community/contribute/code-of-conduct/){:data-ga-name="code of conduct"}{:data-ga-location="feature"}. As noted in our [Statement of Support](/support/#please-dont-use-language-intended-to-threaten-or-harass){:data-ga-name="statement of support"}{:data-ga-location="body"}, we're closing this ticket.
            >
            >Please create a new ticket that complies with our guidelines and one of our support engineers will be happy to assist you.
            >
            >Thank you,

            Repeat violations may result in termination of your support contract and/or business relationship with GitLab.

            #### Please don't send encrypted messages {#please-dont-send-encrypted-messages}
            Some organizations use 3rd party services to encrypt or automatically expire messages. As a matter of security, please do not include any sensitive information in the body or attachments in tickets. All interactions with GitLab Support should be in plain-text.

            GitLab Support Engineers will not click on URLs contained within tickets or interact with these type of 3rd party services to provide support. If you do send such a reply, an engineer will politely ask you to submit your support ticket in plain text, and link you to this section of our Statement of Support.

            If there's a pressing reason for which you need to send along an encrypted file or communication, please discuss it with the Engineer in the ticket.

            #### Please don't send files in formats that can contain executable code {#please-dont-send-files-in-formats-that-can-contain-executable-code}

            Many common file formats, such as Microsoft Office files and PDF, can contain executable code which can be run automatically when the file is opened. As a result, these types of files are often used a computer attack vector.

            GitLab Support Engineers will not download files or interact with these type of files. If you send a file in a format of concern, an engineer will ask you to submit your attachments in an accepted format, and link you to this section of the Support page.

            Examples of acceptable formats include:
               - Plain Text files (.txt, .rtf, .csv, .json, yaml)
               - Images (.jpg, .gif, .png)

            #### Please don't share login credentials {#please-dont-share-login-credentials}
            Do not share login credentials for your GitLab instance to the support team. If the team needs more information about the problem, we will offer to schedule a call with you.

            #### Do not contact Support Engineers directly outside of issues or support cases {#do-not-contact-support-engineers-directly-outside-of-issues-or-support-cases}

            GitLab Support engages with users via the GitLab Support Portals and GitLab.com issues or merge requests only. Using these scoped methods of communication allows for efficiency and collaboration, and ensures both parties' safety while addressing problems that may arise. No attempts should be made to receive support via direct individual email, social media, or other communication methods used by GitLab team members. Use of these unofficial methods to obtain support may be considered abuse of the platform. Repeat violations may result in termination of your support contract and/or business relationship with GitLab.

            If you contact a Support Engineer directly, they'll reply with a short explanation and a link to this paragraph.

            ### Sanitizing data attached to Support Tickets {#sanitizing-data-attached-to-support-tickets}

            If relevant to the problem and helpful in troubleshooting, a GitLab Support Engineer will request information regarding configuration files or logs.

            We encourage customers to sanitize all secrets and private information before sharing them in a GitLab Support ticket.

            Sensitive items that should never be shared include:

            - credentials
            - passwords
            - tokens
            - keys
            - secrets

            There's more specific information on the dedicated [handling sensitive information with GitLab Support](sensitive-information.html){data-ga-name="sensitive information" data-ga-location="body"} page.

            ### Support for GitLab on restricted or offline networks {#support-for-gitlab-on-restricted-or-offline-networks}

            GitLab Support may request logs in Support tickets or ask you to screenshare in customer calls if it would be the most efficient and effective way to troubleshoot and solve the problem.

            Under certain circumstances, sharing logs or screen sharing may be difficult or impossible due to our customers' internal network security policies.

            GitLab Support will never ask our customers to violate their internal security policies, but Support Engineers do not know the details of our customers' internal network security policies.

            In situations where internal or network security policies would prevent you from sharing logs or screen sharing with GitLab Support, please communicate this as early as possible in the ticket so we can adjust the workflows and methods we use to troubleshoot.

            Customer policies preventing the sharing of log or configuration information with Support may lengthen the time to resolution for tickets.

            Customer policies preventing screen sharing during GitLab Support customer calls may impact a Support Engineer's ability to resolve issues during a scheduled call.

            ### Including colleagues in a support ticket {#including-colleagues-in-a-support-ticket}

            Our Support Portal will automatically drop any CCed email addresses for tickets that come in via email. If you would like to include colleagues in a support interaction, there are two ways:

            1. Ask in the ticket to have any number of addresses CCed. This will last for the duration of the ticket, but if you open a new ticket you'll need to request additional participants be added again.
            1. Ask your sales representative or support team in a ticket to submit a request on your behalf to allow others in your organization to view or comment on open support cases.

            To view tickets you have been CCed on, navigate to **Profile Icon > My activities > Requests I'm CC'd on**.

            ### Solving Tickets {#solving-tickets}

            If a customer explains that they are satisfied their concern is being addressed properly in an issue created on their behalf, then the conversation should continue within the issue itself, and GitLab support will close the support ticket. Should a customer wish to reopen a support ticket, they can simply reply to it and it will automatically be reopened.

            ##### Can Users Solve Tickets Themselves? {#can-users-solve-tickets-themselves}
            A user can solve a ticket themselves by logging into the Support Portal and navigating to the [My activities](https://support.gitlab.com/hc/en-us/requests) page. From there the user can select one of their tickets that is in an open or pending state, navigate to the bottom of the ticket notes section, check the `Please consider this request solved` box and press `Submit`. If the user would like to reopen the case they can simply respond with a new comment and the case will reopen or create a follow up case.

            ### GitLab Instance Migration {#gitlab-instance-migration}

            If a customer requests assistance in migrating their existing self-hosted GitLab to a new instance, you can direct them to our [Migrating between two self-hosted GitLab Instances](https://docs.gitlab.com/ee/user/project/import/#migrating-between-two-self-hosted-gitlab-instances){data-ga-name="GitLab instances migration" data-ga-location="body"} documentation. Support will assist with any issues that arise from the GitLab migration. However, the setup and configuration of the new instance, outside of GitLab specific configuration, is considered out of scope and Support will not be able to assist with any resulting issues.

            ### Handling Unresponsive Tickets {#handling-unresponsive-tickets}

            To prevent an accumulation of tickets for which we have not received a response within a specific timescale, we have established the following process. If the ticket owner (our customer) fails to respond within 20 days following our last reply, our ticketing system will mark the ticket as solved. If the ticket owner (our customer) fails to respond within 7 days of the ticket being marked as solved, our ticketing system will proceed to close the ticket.
